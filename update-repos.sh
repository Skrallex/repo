#!/bin/bash

# Colour variables
GREEN="\033[1;32m"
BLUE="\033[1;34m"
RED="\033[1;31m"
YELLOW="\033[1;33m"
MAGENTA="\033[1;35m"
NC="\033[0m"

# Directory variables
TEMP_DIR="pkgs"
PKG_DOWNLOAD_DIR="${TEMP_DIR}/pkg-download"
DEBS_DIR="${TEMP_DIR}/debs"

# Packaging variables
CODENAME="skrallex"
ARCHITECTURES="amd64 armhf i386"
SIGNING_KEY="5F4ED794"


# =============================== DOWNLOAD DEBS ============================== #

echo -e "${GREEN}Downloading latest deb packages${NC}"

#
# Download the deb files
#

mkdir -p "${PKG_DOWNLOAD_DIR}"
cp tmp/*.deb "${PKG_DOWNLOAD_DIR}"

# ============================ EXTRACT SIGNING KEY =========================== #

echo -e "${GREEN}Extracting signing key${NC}"

# Extract the signing key

# ================================ CREATE REPO =============================== #

echo -e "${GREEN}Updating repository${NC}"

# Setup repo configuration
CONF_DIR="${DEBS_DIR}/conf"
mkdir -p "${CONF_DIR}"
touch "${CONF_DIR}"/{option,distributions}

echo "Codename: ${CODENAME}" >> "${CONF_DIR}/distributions"
echo "Components: main" >> "${CONF_DIR}/distributions"
echo "Architectures: ${ARCHITECTURES}" >> "${CONF_DIR}/distributions"
echo "SignWith: ${SIGNING_KEY}" >> "${CONF_DIR}/distributions"

# Generate the deb repo
reprepro -V -b "${DEBS_DIR}" includedeb ${CODENAME} ${PKG_DOWNLOAD_DIR}/*.deb

# ============================ DEPLOY REPO TO GIT ============================ #

echo -e "${GREEN}Deploying repository to Gitlab${NC}"

rm -rf debs && mv "${DEBS_DIR}" .