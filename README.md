# repos

Repository for distribution of packages

## Installation

```
wget -qO - https://gitlab.com/Skrallex/repos/raw/master/pub.gpg | sudo apt-key add -
sudo apt-add-repository 'deb https://gitlab.com/Skrallex/repos/raw/master/debs/ skrallex main'
sudo apt update
```

You can then install reverse-geocoder-lib or reverse-geocoder-cli with:

```
sudo apt install reverse-geocoder-lib
sudo apt install reverse-geocoder-cli
````